public class hw0410 {
    public static void main(String[] args) {
//EX.1
        for (int i = 0; i < 10; i++) {
            System.out.println(i);
            if (i == 5) {
                break;
            }
        }
//EX.2

        for (int i = 0; i < 5; i++) {
            if (i % 2 == 0) {
                System.out.println(i);
            }
        }

//EX.3
        for (int i = 0; i < 10; i++) {
            System.out.println("Hello");
        }

//EX.4
        for (int i = 0; i < 5; i++) {
            if (i % 2 != 0) {
                System.out.println(i);
            }
        }
    }
}
